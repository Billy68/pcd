<?php
if(!empty($_POST) ){
	$linha= $_POST["linha"];
require_once ('conexaodb.php');
$ok = True;
		$sql = "INSERT INTO cadastromaquina(linha) VALUES(?)";
		try {
		$insercao = $conexao->prepare($sql);
		$ok = $insercao->execute(array ($nome));
		}catch(PDOException $r){
			$msg= 'Problemas com o SGBD.'.$r->getMessage();
			$ok = False;
		}catch (Exception $r){//todos as exceções
			$msg = '<p class="alert alert-danger" >  Linha não inserida. Erro.'.$r->getMessage().'</p>';
			$ok= False; 
		}
			if ($ok){
				$msg= '<p class="alert alert-success" > Linha inserida com sucesso.  </p>';
			} else {
				$msg= '<p class="alert alert-danger" > Linha não inserida.  </p>';
			}
			//$msg = $msg.' - '.$ok;
			//echo $msg;
				header('location:pcd.php?mensagem='.$msg);//redireciona para local especificado
	
		}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="utf-8"/>
  <title>Programa PCD</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link href="css/style.css" rel="stylesheet"/>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png"/>
  <link rel="shortcut icon" href="img/favicon.png"/>
  
	</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
					 <a class="navbar-brand" href="PCD.php">Menu</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
					
						<li class="active">
							 <a href="" class="dropdown-toggle" data-toggle="dropdown">Cadastro<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="cadastro.php">Usuario</a>
								</li>
								<li>
									<a href="cadastroproduto.php">Produto</a>
								</li>
								<li>
									<a href="cadastrodefeito.php">Defeito</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="cadastromaquina.php">Maquina</a>
								</li>
								<li>
									<a href="cadastroturno.php">Turno</a>
								</li>
								<li>
									<a href="cadastronivel.php">Nivel</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav">
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Movimento<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="op/incluirordemproducao.php">Ordem de produção</a>
								</li>
								<li>
									<a href="opont/incluirapontamento.php">Apontamento de produção</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav">
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatório<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Produção</a>
								</li>
								<li>
									<a href="#">Defeitos</a>
								</li>
								<li>
									<a href="op/listagemop.php">Ordem de produção</a>
								</li>
								<li>
									<a href="apont/incluirapontamento.php">Apontamento de produção</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Library</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Data
				</li>
			</ul>
			<?php
					if (isset($_GET) and !empty($_GET['mensagem'])){
					echo $_GET ['mensagem'];
										}
			?>
			<div class="page-header">
			</div>
			<form class="form-horizontal" action= "cadastromaquina.php" method="post">
				<fieldset>
					<legend>Cadastro de maquina</legend>
			
					<div class="form-group">
						<label class="col-md-4 control-label" for="linha" >Linha: </label>
						<div class="col-md-4">
							<input type = "text" name="linha" class="form-control input-md"/>
						</div>
					</div>
					<div class="form-group text-center">
						<div class="col-md-8">
							<button type=submit class="btn btn-primary">Gravar </button> 
							
						</div>
					</div>		
				</fieldset>
			</form> 		
		<hr/>
		<div class= "foot well">
		<p>&copy; 2015 -Billy </p>
			
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</div>
</body>
</html>
