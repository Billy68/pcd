<?php
error_reporting(E_ALL);
		ini_set('display_errors','on');
if(!empty($_GET) ){
	$id = $_GET ['id'];
		
		require_once ('conexaodb.php');
		$sql ='SELECT incluirordemproducao.id, incluirordemproducao.ordem_producao, cadastroproduto.descricao, incluirordemproducao.id_produto, incluirordemproducao.quantidade from incluirordemproducao LEFT JOIN cadastroproduto ON incluirordemproducao.id_produto = cadastroproduto.id_produto where id=?';
		$consulta = $conexao->prepare($sql);
		$consulta->execute(array($id));
		$dados = $consulta->fetch(PDO::FETCH_ASSOC);
	
}
?>	

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="utf-8"/>
  <title>Programa PCD</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0/">
  <meta name="description" content=""/>
  <meta name="author" content=""/>

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet"/>
	<link href="css/style.css" rel="stylesheet"/>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png"/>
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png/>
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png/>
  <link rel="shortcut icon" href="img/favicon.png"/>
  

</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
					 <a class="navbar-brand" href="../PCD.php">Menu</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
					
						<li class="active">
							 <a href="" class="dropdown-toggle" data-toggle="dropdown">Cadastro<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="../cadastro.php">Usuario</a>
								</li>
								<li>
									<a href="../cadastroproduto.php">Produto</a>
								</li>
								<li>
									<a href="../cadastrodefeito.php">Defeito</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="../cadastromaquina.php">Maquina</a>
								</li>
								<li>
									<a href="../cadastroturno.php">Turno</a>
								</li>
								<li>
									<a href="../cadastronivel.php">Nivel</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav">
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Movimento<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="incluirordemproducao.php">Ordem de produção</a>
								</li>
								<li>
									<a href="../apont/incluirapontamento.php">Apontamento de produção</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav">
						<li class="dropdown ">
							 <a href="active" class="dropdown-toggle" data-toggle="dropdown">Relatório<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Produção</a>
								</li>
								<li>
									<a href="#">Defeitos</a>
								</li>
								
								<li href="active">
									<a href="listagemop.php">Ordem de produção</a>
								</li>
								<li>
									<a href="../apont/listagemapont.php">Apontamento</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Library</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Data
				</li>
			</ul>
		<?php
			if (isset($_GET) and !empty($_GET['mensagem'])){
				echo $_GET ['mensagem'];
				}
			?>
		
		<div class="page-header">
				
			</div>
			<form class="form-horizontal" action= "alteraop.php" method="post">
				<input type = "hidden" name="id" value="<?php echo $id; ?>"/>
				<fieldset>
					<legend>Visualisa dados da Ordem de produção.</legend>
			
					<div class="form-group">
						<label class="col-md-4 control-label" for="ordem_producao" >Ordem Produção: </label>
						<div class="col-md-4">
							<input type = "text" name="ordem_producao" class="form-control input-md" value="<?php echo $dados['ordem_producao']; ?>"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label" for="descricao" >Produto: </label>
						<div class="col-md-4">
							<input type = "text" name="descricao" class="form-control input-md" value="<?php echo $dados['descricao']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="quantidade" >Quantidade: </label>
						<div class="col-md-4">
							<input type = "text" name="quantidade" class="form-control input-md" value="<?php echo $dados['quantidade']; ?>"/>
						</div>
					</div>
					
					<div class="form-group text-center">
						<div class="col-md-8">
							<a href="listagemop.php" class="btn btn-primary">VOLTAR </a> 
						</div>
					</div>		
				</fieldset>
			</form>	
	
		<div class= "foot well">
		<p>&copy; 2015 -Billy </p>
			
		</div>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</div>
</body>
</html>
